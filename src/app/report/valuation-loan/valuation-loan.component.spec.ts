import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValuationLoanComponent } from './valuation-loan.component';

describe('ValuationLoanComponent', () => {
  let component: ValuationLoanComponent;
  let fixture: ComponentFixture<ValuationLoanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValuationLoanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValuationLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
