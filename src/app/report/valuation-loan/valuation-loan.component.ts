import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReportService } from '../report.service';
import { IValuationLoan } from '../report';

@Component({
  selector: 'app-valuation-loan',
  templateUrl: './valuation-loan.component.html',
  styleUrls: ['./valuation-loan.component.css'],
})
export class ValuationLoanComponent implements OnInit {
  title = 'Valuation report';
  report: IValuationLoan;

  constructor(
    private reportService: ReportService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.reportService.getValuationLoan().subscribe({
      next: (report) => {
        this.report = report;
      },
      error: (err) => {
        this._snackBar.open('ERROR!', 'Close', {
          duration: 5000,
        });
      },
    });
  }
}
