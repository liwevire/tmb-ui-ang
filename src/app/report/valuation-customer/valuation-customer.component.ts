import { Component, OnInit, ViewChild } from '@angular/core';
import { IValuationCustomer } from '../report';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortable, SortDirection } from '@angular/material/sort';
import { ReportService } from '../report.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-valuation-customer',
  templateUrl: './valuation-customer.component.html',
  styleUrls: ['./valuation-customer.component.css']
})
export class ValuationCustomerComponent implements OnInit {
  title = 'Valuation by Customer';
  valuationCustomers: IValuationCustomer[] = [];
  displayedColumns: string[] = [
    'id',
    'name',
    'loanCounter',
    'principal',
    'interest',
    'valuation',
    'valuationPrcnt',
  ];
  dataSource; any;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private reportService: ReportService,
    private _snackBar: MatSnackBar
  ) {}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    this.reportService.getValuationCustomer().subscribe({
      next: (valuationCustomers) => {
        this.valuationCustomers = valuationCustomers;
        this.transformValuationCustomers(this.valuationCustomers);
        this.dataSource = new MatTableDataSource<IValuationCustomer>(this.valuationCustomers);
        this.dataSource.paginator = this.paginator;
        this.sort.sort({
          id: 'valuationprcnt',
          start: 'asc'
        } as MatSortable); 
        this.sort.direction = 'asc' as SortDirection;
        this.dataSource.sort = this.sort;
      },
      error: (err) => {
        this._snackBar.open('ERROR!', 'Close', {
          duration: 5000,
        });
      },
    });
  }

  transformValuationCustomers(valuationCustomers: IValuationCustomer[]){
    valuationCustomers.forEach(valCust => {
      valCust.principal = valCust.loanOutstandingResponse.principal
      valCust.principalPaid = valCust.loanOutstandingResponse.principalPaid
      valCust.principalOutstanding = valCust.loanOutstandingResponse.principalOutstanding
      valCust.interest = valCust.loanOutstandingResponse.interest
      valCust.interestPaid = valCust.loanOutstandingResponse.interestPaid
      valCust.interestOutstanding = valCust.loanOutstandingResponse.interestOutstanding
      valCust.totalDays = valCust.loanOutstandingResponse.totalDays
      valCust.calcComment = valCust.loanOutstandingResponse.calcComment
      valCust.valuation = valCust.loanOutstandingResponse.valuation
      valCust.valuationPrcnt = valCust.loanOutstandingResponse.valuationPrcnt
    })
  }
}