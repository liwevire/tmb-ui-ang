import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValuationCustomerComponent } from './valuation-customer.component';

describe('ValuationCustomerComponent', () => {
  let component: ValuationCustomerComponent;
  let fixture: ComponentFixture<ValuationCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValuationCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValuationCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
