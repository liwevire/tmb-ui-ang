import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InceptionReportComponent } from './inception-report.component';

describe('InceptionReportComponent', () => {
  let component: InceptionReportComponent;
  let fixture: ComponentFixture<InceptionReportComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InceptionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InceptionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
