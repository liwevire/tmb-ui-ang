import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DatedReportComponent } from './dated-report.component';

describe('DatedReportComponent', () => {
  let component: DatedReportComponent;
  let fixture: ComponentFixture<DatedReportComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DatedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
