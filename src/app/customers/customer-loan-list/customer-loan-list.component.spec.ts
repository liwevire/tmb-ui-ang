import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CustomerLoanListComponent } from './customer-loan-list.component';

describe('CustomerLoanListComponent', () => {
  let component: CustomerLoanListComponent;
  let fixture: ComponentFixture<CustomerLoanListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerLoanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerLoanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
