import { Component, OnInit, ViewChild, Input, OnChanges } from '@angular/core';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, MatSortable, SortDirection } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

import { LoanService } from '../loan.service';
import { ILoan, getPrincipalActivity } from '../loan';
import { IActivity } from '../activity';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.css'],
})
export class LoanListComponent implements OnInit, OnChanges {
  title = 'Loans';
  errorMessage: string;
  @Input() loans: ILoan[] = [];
  @Input() loadLoans: boolean = true;
  displayedColumns: string[] = [
    'id',
    'altId',
    'date',
    'name',
    'post',
    'principal',
    'status',
    'weight',
    'comment',
  ];
  dataSource: any;
  loanType: string = 'open';
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private loanService: LoanService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {}

  getPrincipalActivity(activities: IActivity[]): IActivity {
    return getPrincipalActivity(activities) as IActivity;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params)=>{
      if(params.get('loanType')!=null)
        this.loanType = params.get('loanType')
      if (this.loadLoans){
        this.dataSource = null;
        this.loanService.getLoans(this.loanType).subscribe({
          next: (loans) => {
            this.title = "Loans - " + this.loanType;
            this.loans = loans;
            this.dataSource = new MatTableDataSource<ILoan>(this.loans);
            this.dataSource.paginator = this.paginator;
            this.sort.sort({
              id: 'id',
              start: 'desc',
            } as MatSortable);
            this.sort.direction = 'desc' as SortDirection;
            this.dataSource.sort = this.sort;            
          },
          error: (err) => {
            this._snackBar.open('ERROR!', 'Close', {
              duration: 5000,
            });
          },
        });
      }
    })    
  }
  ngOnChanges(): void {
    if (!this.loadLoans) {
      this.dataSource = new MatTableDataSource<ILoan>(this.loans);
      this.dataSource.paginator = this.paginator;
      
      this.dataSource.sort = this.sort; 
    }
  }
}
