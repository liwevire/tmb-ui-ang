import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReportService } from './report/report.service';
import { IGoldRate, emptyGoldRate } from './report/report';
import { Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'tmb-ui-ang';
  report: IGoldRate = { ...emptyGoldRate };
  constructor(
    private reportService: ReportService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.reportService.getGoldRate().subscribe({
      next: (report) => {
        this.report = report;
      },
      error: (err) => {
        this._snackBar.open('ERROR!', 'Close', {
          duration: 5000,
        });
      },
    });
    //this.router.navigate(['/loans/open'])
  }
}
